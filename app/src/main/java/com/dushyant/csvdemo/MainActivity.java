package com.dushyant.csvdemo;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    TextView txt;
    Button btn;
    Intent myintent;
    InputStream inputStream;
    String[] data;
    AssetManager assetManager = MainActivity.this.getAssets();
    String paht;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btn = findViewById(R.id.btm);
        txt = findViewById(R.id.txt);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myintent = new Intent(Intent.ACTION_GET_CONTENT);
                myintent.setType("*/*");
                startActivityForResult(myintent, 10);


            }
        });

        try {
            InputStream csvStream = assetManager.open(paht);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                data = csvLine.split(",");
                try {

                    Log.e("Data ", "" + data[0]);
                    //Log.e("Data ",""+data[0]+""+data[1]+""+data[2]) ;

                } catch (Exception e) {
                    Log.e("Problem", e.toString());
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK) {
                    paht = data.getData().getPath();
                    txt.setText(paht);
                }

                break;
        }
    }


}